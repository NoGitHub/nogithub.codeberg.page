+++
title = "Please don't upload my code on GitHub"
+++

<div align="center"><img src="./icon.svg" width="128" height="128"></div>

# Please don't upload my code to GitHub

_This is a call to open source developers to **not** upload the work of others to GitHub._

## What's the problem with GitHub?

Well, there are **a lot** of problems with GitHub. Here, we will mainly focus on a feature called _Copilot_, but if you're interested to learn more about the others, [please visit this page](https://sfconservancy.org/GiveUpGitHub/).

## What is Copilot?

According to GitHub,

> GitHub Copilot is an AI pair programmer that helps you write code faster and with less work. It draws context from comments and code to suggest individual lines and whole functions instantly.\
--- [https://github.com/features/copilot](https://web.archive.org/web/20221017065610/https://github.com/features/copilot)

However, we know that an AI needs to be trained on a huge data sample in order to work. In that case, Copilot learns by scanning GitHub repositories:

> GitHub Copilot is trained on billions of lines of public code. The suggestions it makes to you are adapted to your code, but the processing behind it is ultimately informed by code written by others.\
--- [https://github.blog/2021-06-30-github-copilot-research-recitation/](https://web.archive.org/web/20221017082203/https://github.blog/2021-06-30-github-copilot-research-recitation/)

The problem is that this so-called _public code_ is not always under _public domain_ dedication. Sometimes, the code is licensed under a copyleft license (such as [GPL](https://www.gnu.org/licenses/gpl-3.0.html)), which requires any modification of the source code to be redistributed under the same license as the original. Sometimes the code is licensed under a permissive license (such as [ISC](https://en.wikipedia.org/wiki/ISC_license)) which at the very least requires the inclusion of authorship information and license text.

However, when Copilot suggests pieces of code, [it doesn't include any notice of the original author, nor does it mention anything about the license](https://web.archive.org/web/20221017081115/https://nitter.net/docsparse/status/1581461734665367554). This can lead [to some copylefted code being included in projects that do not have the legal right to use it](https://sfconservancy.org/blog/2022/feb/03/github-copilot-copyleft-gpl/) (such as proprietary projects, or projects that are not copyleft licensed). This is a violation of both the license terms and the intellectual property of the authors of the original code. 

We are tired of this legal abuse and we want it to stop right now! Microsoft has been attacking copyleft licensing for years. We want that to stop. We would like to protect our work.

## So, just don't put your code on GitHub, what's the big deal?

The issue is bigger than it seems. Even if a project is not hosted on GitHub, other people have the legal right (depending on the license) to redistribute the source code. It means that they have the right to share the code of others on GitHub, as long as they respect the terms of license. This is totally legal. But then, Copilot will be able to analyze the code and possibly violate the license terms, which isn't.

This is why we ask you, as other developers of the open source community, **not to upload our code to GitHub**.

## Is this a legal document?

No, it isn't. If the project is under an open source license, it means that everyone can share a copy -- even on GitHub -- of the licensed material under certain conditions. A license restricting this right wouldn't be open source anymore. However, since GitHub may not respect the terms of licensed code that is hosted on their servers, not uploading the code of others there is, in fact, a deeply ethical choice.

## How to ask others to not share my code on GitHub?

If you want, you can add our badge to your website or to your repository's README:

[![Please don't upload to GitHub](/badge.svg)](https://nogithub.codeberg.page)

**Asciidoc:**
```asciidoc
[#badge,link=https://nogithub.codeberg.page]
image::https://nogithub.codeberg.page/badge.svg[Please don't upload to GitHub,120,100]
```

**Markdown:**
```markdown
[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)
```

**HTML:**
```html
<a href="https://nogithub.codeberg.page"><img src="https://nogithub.codeberg.page/badge.svg" alt="Please don't upload to GitHub"></a>
```
