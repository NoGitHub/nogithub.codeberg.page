# No GitHub

Please visit https://nogithub.codeberg.page for more information.

## Licenses

- This repository is licensed under [CC-BY-SA-4.0](https://codeberg.org/NoGitHub/nogithub.codeberg.page/src/branch/main/LICENSE)
- It uses the theme [Hugo Bear Blog](https://github.com/janraasch/hugo-bearblog/), which is licensed under [MIT license](https://github.com/janraasch/hugo-bearblog/blob/master/LICENSE)
- The logo is licensed under [CC-BY-SA-4.0](https://codeberg.org/NoGitHub/nogithub.codeberg.page/src/branch/main/LICENSE) as well
- The GitHub icon used in the logo [is authored by Font Awesome](https://fontawesome.com) and is licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by/4.0/deed.en)
